#version 140

in vec3 pass_Color;
in vec2 pass_TexCoord;

out vec4 out_Color;

uniform sampler2D unform_texture;

void main(void) {
     vec2 invertedTexCoord = vec2(pass_TexCoord.x, 1 - pass_TexCoord.y);;
     out_Color = vec4(pass_Color, 1.0);
//     out_Color = texture(uniform_texture, invertedTexCoord);
}
