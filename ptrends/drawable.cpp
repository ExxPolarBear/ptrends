/ Periodic Table Visualizer
// renderer.cpp
//

#include <iostream>

#include "objects.h"

Drawable::Drawable( ) {
	vboLoaded = false;
	verticies.clear();
	modelMatrix = glm::mat4(1.0);
}

Drawable::~Drawable( ) {
	if(vboLoaded) {
		UnloadVbo();
	}
}

void Drawable::LoadVbo( ) {
	if(verticies.size( ) > 0) {
		glGenBuffers(1, &vboVerticies);
		glBindBuffer(GL_ARRAY_BUFFER, vboVerticies);
		glBufferData(GL_ARRAY_BUFFER, verticies.size( ) * sizeof(verticies[0]), 
			verticies.data( ), GL_STATIC_DRAW);

	}
}

void Drawable::UnloadVbo( ) {
	glDeleteBuffers(1, &vboVerticies);
}

Material::Material( ) {
	vboLoaded = false;
	colors.clear();
}

Material::~Material( ) {
	if(vboLoaded) {
		UnloadVbo();
	}
}

void Material::LoadVbo( ) {
	if(verticies.size( ) > 0) {
		glGenBuffers(1, &vboColors);
		glBindBuffer(GL_ARRAY_BUFFER, vboColors);
		glBufferData(GL_ARRAY_BUFFER, colors.size( ) * sizeof(colors[0]), 
			colors.data( ), GL_STATIC_DRAW);
	}
}

void Material
