#include <iostream>

#include "context.h"

using namespace std;

OpenGLContext::OpenGLContext(void) {
	isInit = false;
}

OpenGLContext::OpenGLContext(HWND hWnd) {
	isInit = false;
	CreateContext(hWnd);
}

OpenGLContext::~OpenGLContext(void) {
	if(isInit == true) {
		ReleaseContext();
	}
}

bool OpenGLContext::CreateContext(HWND hWnd) {
	this->hWnd = hWnd; // Set the HWND for our window

	hDC = GetDC(hWnd); // Get the device Context for our window

	PIXELFORMATDESCRIPTOR pfd; // Create a new PIXELFORMATDESCRIPTOR (PFD)
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR)); // Clear our  PFD
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR); // Set the size of the PFD to the size of the class
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW; // Enable double buffering, opengl support and drawing to a window
	pfd.iPixelType = PFD_TYPE_RGBA; // Set our application to use RGBA pixels
	pfd.cColorBits = 32; // Give us 32 bits of color information (the higher, the more colors)
	pfd.cDepthBits = 32; // Give us 32 bits of depth information (the higher, the more depth levels)
	pfd.iLayerType = PFD_MAIN_PLANE; // Set the layer of the PFD

	int nPixelFormat = ChoosePixelFormat(hDC, &pfd); // Check if our PFD is valid and get a pixel format back
	if (nPixelFormat == 0) // If it fails
			return false;

	BOOL bResult = SetPixelFormat(hDC, nPixelFormat, &pfd); // Try and set the pixel format based on our PFD
	if (!bResult) // If it fails
		return false;

	HGLRC tempOpenGLContext = wglCreateContext(hDC); // Create an OpenGL 2.1 Context for our device Context
	wglMakeCurrent(hDC, tempOpenGLContext); // Make the OpenGL 2.1 Context current and active

	GLenum error = glewInit(); // Enable GLEW
	if (error != GLEW_OK) // If GLEW fails
		return false;

	int attributes[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3, // Set the MAJOR version of OpenGL to 2
		WGL_CONTEXT_MINOR_VERSION_ARB, 1, // Set the MINOR version of OpenGL to 1
		WGL_CONTEXT_FLAGS_ARB, 0, // Set our OpenGL Context to be forward compatible
		0
	};
	
	if (wglewIsSupported("WGL_ARB_create_Context") == 1) { // If the OpenGL 3.x Context creation extension is available
		hRC = wglCreateContextAttribsARB(hDC, NULL, attributes); // Create and OpenGL 3.x Context based on the given attributes
		wglMakeCurrent(NULL, NULL); // Remove the temporary Context from being active
		wglDeleteContext(tempOpenGLContext); // Delete the temporary OpenGL 2.1 Context
		wglMakeCurrent(hDC, hRC); // Make our OpenGL 3.0 Context current
	}
	else {
		hRC = tempOpenGLContext; // If we didn't have support for OpenGL 3.x and up, use the OpenGL 2.1 Context
	}

	cout << "Using OpenGL: " << glGetString(GL_VERSION) << endl;
	cout << "OpenGL installation: " << glGetString(GL_VENDOR) << endl;

	isInit = true;
	return true; // We have successfully created a Context, return true
}

void OpenGLContext::ReleaseContext(void) {
	wglMakeCurrent(hDC, 0);
	wglDeleteContext(hRC);

	ReleaseDC(hWnd, hDC);
	isInit = false;
}

bool OpenGLContext::IsInit() {
	return isInit;
}

HDC OpenGLContext::GetDeviceContext(void) {
	return hDC;
}