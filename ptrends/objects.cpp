// Periodic Table Visualizer
// objects.cpp
//

#include "objects.h"

// ****************************************************************************
// Class SolidColor implementations

SolidColor::SolidColor() { };

SolidColor::SolidColor(glm::vec4 color, int nVerticies) {
	this->nVerticies = nVerticies;
	for(int index = 0; index < nVerticies; index++) {
		verticies.push_back(color);
	}
}

SolidColor::~SolidColor( ) {
}

// ****************************************************************************
// Class TestColors implementations

TestBoxColors::TestBoxColors( ) {
	nVerticies = 36;
	for(int index = 0; index < nVerticies; index++) {
		verticies.push_back(GetVertex(index));
	}
}

TestBoxColors::~TestBoxColors( ) {
}

glm::vec4 TestBoxColors::GetVertex(int index) {
	if(index == 0)
		return glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	if(index == 1)
		return glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	if(index == 2)
		return glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	if(index == 3)
		return glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	if(index == 4)
		return glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	if(index == 5)
		return glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	
	if(index == 6)
		return glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	if(index == 7)
		return glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	if(index == 8)
		return glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	
	if(index == 9)
		return glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	if(index == 10)
		return glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	if(index == 11)
		return glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	
	if(index == 12)
		return glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	if(index == 13)
		return glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	if(index == 14)
		return glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	
	if(index == 15)
		return glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	if(index == 16)
		return glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	if(index == 17)
		return glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);

	if(index == 18)
		return glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
	if(index == 19)
		return glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
	if(index == 20)
		return glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);

	if(index == 21)
		return glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
	if(index == 22)
		return glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
	if(index == 23)
		return glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);

	if(index == 24)
		return glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
	if(index == 25)
		return glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
	if(index == 26)
		return glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);

	if(index == 27)
		return glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
	if(index == 28)
		return glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);
	if(index == 29)
		return glm::vec4(0.0f, 1.0f, 1.0f, 1.0f);

	if(index == 30)
		return glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
	if(index == 31)
		return glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
	if(index == 32)
		return glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);

	if(index == 33)
		return glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
	if(index == 34)
		return glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
	if(index == 35)
		return glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);

	return glm::vec4(0.0f);
}

// ****************************************************************************
// Class DiscreteTexture implementations

DiscreteTexture::DiscreteTexture() {
	nCooridinates = 6;
	for(int index = 0; index < nCooridinates; index++) {
		cooridinates.push_back(GetVertex(index));
	}
}

DiscreteTexture::DiscreteTexture(TextureData* texture) {
	nCooridinates = 6;
	for(int index = 0; index < nCooridinates; index++) {
		cooridinates.push_back(GetVertex(index));
	}
	data = new TextureData(*texture);
	tbo.Load(data);
	vbo.Load(nCooridinates, cooridinates.data());
}

DiscreteTexture::~DiscreteTexture() {
	tbo.Unload();
	delete data;
	vbo.Unload();
}

glm::vec2 DiscreteTexture::GetVertex(int index) {
	if(index == 0)
		return glm::vec2(1.0f, 1.0f);
	if(index == 1)
		return glm::vec2(0.0f, 1.0f);
	if(index == 2)
		return glm::vec2(0.0f, 0.0f);

	if(index == 3)
		return glm::vec2(1.0f, 1.0f);
	if(index == 4)
		return glm::vec2(0.0f, 0.0f);
	if(index == 5)
		return glm::vec2(1.0f, 0.0f);

	return glm::vec2(0.0f);
}

// ****************************************************************************
// Class TextTexture implementations

TextTexture::TextTexture() {
	FT_Library library;
	FT_Face face;

	if(FT_Init_FreeType(&library)) {

	}

	if(FT_New_Face(library, "C:\\Windows\\Fonts\\Tahoma.ttf", 0, &face)) {
	}


}

TextTexture::~TextTexture() {
}


// ****************************************************************************
// Class BoxMesh implementations

BoxMesh::BoxMesh( ) {
	nVerticies = 36;
	for(int index = 0; index < nVerticies; index++) {
		verticies.push_back(GetVertex(index, glm::vec3(1.0f)));
	}
}

BoxMesh::BoxMesh(glm::vec3 scale) {
	nVerticies = 36;
	for(int index = 0; index < nVerticies; index++) {
		verticies.push_back(GetVertex(index, scale));
	}
}

BoxMesh::~BoxMesh( ) {
}

glm::vec3 BoxMesh::GetVertex(int index, glm::vec3 scale) {
	if(index == 0)
		return glm::vec3(0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 1)
		return glm::vec3(-0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 2)
		return glm::vec3(-0.5f, -0.5f, 0.5f) * glm::vec3(scale);

	if(index == 3)
		return glm::vec3(0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 4)
		return glm::vec3(-0.5f, -0.5f, 0.5f) * glm::vec3(scale);
	if(index == 5)
		return glm::vec3(0.5f, -0.5f, 0.5f) * glm::vec3(scale);
	
	if(index == 6)
		return glm::vec3(0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 7)
		return glm::vec3(0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 8)
		return glm::vec3(0.5f, -0.5f, 0.5f) * glm::vec3(scale);
	
	if(index == 9)
		return glm::vec3(0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 10)
		return glm::vec3(0.5f, -0.5f, 0.5f) * glm::vec3(scale);
	if(index == 11)
		return glm::vec3(0.5f, -0.5f, -0.5f) * glm::vec3(scale);
	
	if(index == 12)
		return glm::vec3(-0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 13)
		return glm::vec3(0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 14)
		return glm::vec3(0.5f, -0.5f, -0.5f) * glm::vec3(scale);
	
	if(index == 15)
		return glm::vec3(-0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 16)
		return glm::vec3(0.5f, -0.5f, -0.5f) * glm::vec3(scale);
	if(index == 17)
		return glm::vec3(-0.5f, -0.5f, -0.5f) * glm::vec3(scale);

	if(index == 18)
		return glm::vec3(-0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 19)
		return glm::vec3(-0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 20)
		return glm::vec3(-0.5f, -0.5f, -0.5f) * glm::vec3(scale);

	if(index == 21)
		return glm::vec3(-0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 22)
		return glm::vec3(-0.5f, -0.5f, -0.5f) * glm::vec3(scale);
	if(index == 23)
		return glm::vec3(-0.5f, -0.5f, 0.5f) * glm::vec3(scale);

	if(index == 24)
		return glm::vec3(0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 25)
		return glm::vec3(0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 26)
		return glm::vec3(-0.5f, 0.5f, -0.5f) * glm::vec3(scale);

	if(index == 27)
		return glm::vec3(0.5f, 0.5f, 0.5f) * glm::vec3(scale);
	if(index == 28)
		return glm::vec3(-0.5f, 0.5f, -0.5f) * glm::vec3(scale);
	if(index == 29)
		return glm::vec3(-0.5f, 0.5f, 0.5f) * glm::vec3(scale);

	if(index == 30)
		return glm::vec3(0.5f, -0.5f, 0.5f) * glm::vec3(scale);
	if(index == 31)
		return glm::vec3(-0.5f, -0.5f, 0.5f) * glm::vec3(scale);
	if(index == 32)
		return glm::vec3(-0.5f, -0.5f, -0.5f) * glm::vec3(scale);

	if(index == 33)
		return glm::vec3(0.5f, -0.5f, 0.5f) * glm::vec3(scale);
	if(index == 34)
		return glm::vec3(-0.5f, -0.5f, -0.5f) * glm::vec3(scale);
	if(index == 35)
		return glm::vec3(0.5f, -0.5f, -0.5f) * glm::vec3(scale);

	return glm::vec3(0.0f);
}

// ****************************************************************************
// Class RectMesh implementations

RectMesh::RectMesh() {
	nVerticies = 6;
	for(int index = 0; index < nVerticies; index++) {
		verticies.push_back(GetVertex(index, glm::vec2(1.0f)));
	}
}

RectMesh::RectMesh(glm::vec2 scale) {
	nVerticies = 6;
	for(int index = 0; index < nVerticies; index++) {
		verticies.push_back(GetVertex(index, scale));
	}
}

RectMesh::~RectMesh() {
}

glm::vec3 RectMesh::GetVertex(int index, glm::vec2 scale) {
	if(index == 0)
		return glm::vec3(0.5f, 0.5f, 0.0f) * glm::vec3(scale, 1.0f);
	if(index == 1)
		return glm::vec3(-0.5f, 0.5f, 0.0f) * glm::vec3(scale, 1.0f);
	if(index == 2)
		return glm::vec3(-0.5f, -0.5f, 0.0f) * glm::vec3(scale, 1.0f);
	
	if(index == 3)
		return glm::vec3(0.5f, 0.5f, 0.0f) * glm::vec3(scale, 1.0f);
	if(index == 4)
		return glm::vec3(-0.5f, -0.5f, 0.0f) * glm::vec3(scale, 1.0f);
	if(index == 5)
		return glm::vec3(0.5f, -0.5f, 0.0f) * glm::vec3(scale, 1.0f);

	return glm::vec3(0.0f);
}

// ****************************************************************************
// Class ColoredBox implementations

ColoredBox::ColoredBox( ) {
	mesh = new BoxMesh( );
	material = new SolidColor(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f), mesh->GetNumVerticies( ));
	texture = NULL;
	modelMatrix = glm::mat4(1.0f);
	primitiveType = GL_TRIANGLES;
}

ColoredBox::ColoredBox(glm::vec3 scale, glm::vec3 translation, glm::vec4 color) {
	mesh = new BoxMesh(scale);
	material = new SolidColor(color, mesh->GetNumVerticies( ));
	texture = NULL;
	modelMatrix = glm::translate(glm::mat4(1.0f), translation);
	primitiveType = GL_TRIANGLES;
}

ColoredBox::~ColoredBox( ) {
	delete mesh;
	delete material;
}

// ****************************************************************************
// ColoredRect : Rectangular object with solid color

ColoredRect::ColoredRect() {
	mesh = new RectMesh();
	material = new SolidColor(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f), mesh->GetNumVerticies());
	texture = NULL;
	modelMatrix = glm::mat4(1.0f);
	primitiveType = GL_TRIANGLES;
}

ColoredRect::ColoredRect(glm::vec2 scale, glm::vec3 translation, glm::vec4 color) {
	mesh = new RectMesh(scale);
	material = new SolidColor(color, mesh->GetNumVerticies());
	texture = NULL;
	modelMatrix = glm::translate(glm::mat4(1.0f), translation);
	primitiveType = GL_TRIANGLES;
}

ColoredRect::~ColoredRect() {
	delete mesh;
	delete material;
}

// ****************************************************************************
// Class TestBox implementations

TestBox::TestBox() {
	mesh = new BoxMesh();
	material = new TestBoxColors();
	texture = NULL;
	modelMatrix = glm::mat4(1.0f);
	primitiveType = GL_TRIANGLES;
}

TestBox::TestBox(glm::vec3 scale, glm::vec3 translation) {
	mesh = new BoxMesh(scale);
	material = new TestBoxColors();
	texture = NULL;
	modelMatrix = glm::translate(glm::mat4(1.0f), translation);
	primitiveType = GL_TRIANGLES;
}

TestBox::~TestBox( ) {
	delete mesh;
	delete material;
}

// ****************************************************************************
// Class TexturedRect implementations

TexturedRect::TexturedRect() {
	mesh = new RectMesh();
	material = new SolidColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), 6);
	texture = new DiscreteTexture();
	modelMatrix = glm::mat4(1.0f);
	primitiveType = GL_TRIANGLES;
}

TexturedRect::TexturedRect(glm::vec2 scale, glm::vec3 translation,
	TextureData* tex) {
	mesh = new RectMesh(scale);
	material = new SolidColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), 6);
	texture = new DiscreteTexture(tex);
	modelMatrix = glm::translate(glm::mat4(1.0f), translation);
	primitiveType = GL_TRIANGLES;
}

TexturedRect::~TexturedRect( ) {
	delete mesh;
	delete material;
	delete texture;
}

// ****************************************************************************
// Class Text implementations

TextObject::TextObject() {
	mesh = new RectMesh();
	material = new SolidColor(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), 6);
	texture = new TextTexture();
	modelMatrix = glm::mat4(1.0f);
	primitiveType = GL_TRIANGLES;
}

TextObject::~TextObject() {
	delete mesh;
	delete material;
	delete texture;
}