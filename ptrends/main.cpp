// Periodic Table Visualizer
// main.cpp
//

#include <Windows.h>
#include <stdio.h>

// OpenGL and GLEW include files
#include <gl/glew.h>
#include <gl/wglew.h>
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

// GLM include files
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Free Image Loader
#include <FreeImage.h>

#include "renderer.h"
// #include "test.c"
//#include "res_texture.c"

OpenGLRenderer renderer;
HINSTANCE hInstance;

LRESULT CALLBACK WinProc(HWND hWindow, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message) {
	case WM_LBUTTONDOWN: {
		/*char fileName[MAX_PATH];
		HINSTANCE hInstance = GetModuleHandle(NULL);

		GetModuleFileName(hInstance, fileName, MAX_PATH);
		MessageBox(hWindow, fileName, "This program is: ", MB_OK | MB_ICONINFORMATION);*/
		
		char text[64];
		sprintf_s(text, "%d, %d", renderer.GetWindowWidth( ), renderer.GetWindowHeight( ));

		MessageBox(hWindow, text, "Window Dimentions",  MB_OK | MB_ICONINFORMATION);
	}
		break;
	case WM_SIZE:
		renderer.ReshapeWindow(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hWindow);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWindow, message, wParam, lParam);
	}
	return 0;
}

bool OpenWindow(LPCSTR title, int width, int height) {
	WNDCLASS windowClass;
	HWND hWnd;
	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	hInstance = GetModuleHandle(NULL);

	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = (WNDPROC) WinProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = NULL;
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = title;

	if(!RegisterClass(&windowClass)) {
		return false;
	}

	hWnd = CreateWindowEx(dwExStyle, title, title, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, width, height, NULL, NULL, hInstance, NULL);

	if(!renderer.CreateContext(hWnd)) {
		return false;
	}

	ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);

	renderer.ReshapeWindow(width, height);

	return true;
}

void FreeImageErrorHandler(FREE_IMAGE_FORMAT format, const char *message) {
	if(format != FIF_UNKNOWN) {
		printf("%s format\n", FreeImage_GetFormatFromFIF(format));
	}
	printf("%s\n", message);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	MSG message;
	
	FreeImage_Initialise();
	FreeImage_SetOutputMessage(FreeImageErrorHandler);

	char *title = "Periodic Trends Visualizer";
	OpenWindow(title, 516, 538);
	
	renderer.SetupScene( );
	renderer.ReshapeWindow(500, 500);

	char *filename = "test.bmp";

	FREE_IMAGE_FORMAT imgFormat = FreeImage_GetFileType(filename, 0);
	if(imgFormat == FIF_UNKNOWN) {
		FreeImage_DeInitialise();
		return 0;
	}
	FIBITMAP *image = FreeImage_Load(imgFormat, filename, 0);
	if(image == NULL) {
		FreeImage_DeInitialise();
		return 0;
	}
	TextureData data(FreeImage_GetWidth(image), FreeImage_GetHeight(image),
		3, FreeImage_GetBits(image));

	Scene scene;
	scene.AddObject(new TestBox(glm::vec3(2.0f, 1.0f, 0.5f),
		glm::vec3(1.0f, 1.0f, 1.0f)));
	scene.AddObject(new ColoredBox(glm::vec3(1.0f, 1.0f, 1.0f), 
		glm::vec3(-1.0f, -1.0f, -1.0f), glm::vec4(0.2f, 0.45f, 0.2f, 1.0f)));
	scene.AddObject(new ColoredRect(glm::vec2(2.0f, 0.33f), 
		glm::vec3(0.0f, 0.0f, 1.0f), glm::vec4(1.0f, 0.5f, 0.7f, 1.0f)));
	scene.AddObject(new TexturedRect(glm::vec2(1.0f), glm::vec3(1.0f, -1.0f, -1.0f), 
		&data));
	
	FreeImage_Unload(image);
	FreeImage_DeInitialise();

	bool running = true;
	
	while(running) {
		if(PeekMessage(&message, NULL, 0, 0, PM_REMOVE)) {
			if(message.message == WM_QUIT) {
				running = false;
			}
			else {
				TranslateMessage(&message);
				DispatchMessage(&message);
			}
		}
		else {
			renderer.ClearScreen();
			renderer.DrawScene(&scene);
			renderer.SwapBuffer();
		}
	}
	return message.wParam;
}