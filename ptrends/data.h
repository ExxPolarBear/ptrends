// Periodic Table Visualizer
// data.h
//

#include <string>

struct ElementData {

	char *name;
	char *symbol;
	unsigned int number;

	float atomicWeight;
	float atomicRadius;
	float ionizationEnergy;
	float electronAffinity;
	float electronegativity;

};

