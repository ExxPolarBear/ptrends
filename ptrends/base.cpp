// Periodic Table Visualizer
// base.cpp
//

#include "base.h"

// ****************************************************************************
// Class TextureData implementations

TextureData::TextureData() {
	SetWidth(0);
	SetHeight(0);
	SetBytesPerPixel(0);
	pixelData = new unsigned char;
}

TextureData::TextureData(TextureData &texture) {
	SetWidth(texture.GetWidth());
	SetHeight(texture.GetHeight());
	SetBytesPerPixel(texture.GetBytesPerPixel());
	SetPixelData(texture.GetPixelData());
}

TextureData::TextureData(unsigned int w, unsigned int h, unsigned int bpp,
	unsigned char *data) {
	SetWidth(w);
	SetHeight(h);
	SetBytesPerPixel(bpp);
	SetPixelData(data);
}

TextureData::~TextureData() {
	delete pixelData;
}

void TextureData::SetWidth(unsigned int w) {
	width = w;
}

void TextureData::SetHeight(unsigned int h) {
	height = h;
}

void TextureData::SetBytesPerPixel(unsigned int bpp) {
	bytesPerPixel = bpp;
}

void TextureData::SetPixelData(unsigned char *data) {
	int size = width*height*bytesPerPixel;
	pixelData = new unsigned char[size];
	memcpy_s(pixelData, size, data, size);
}

void TextureData::Set(TextureData* const texture) {
	SetWidth(texture->GetWidth());
	SetHeight(texture->GetHeight());
	SetBytesPerPixel(texture->GetBytesPerPixel());
	SetPixelData(texture->GetPixelData());
}

unsigned int TextureData::GetWidth() {
	return width;
}

unsigned int TextureData::GetHeight() {
	return height;
}

unsigned int TextureData::GetBytesPerPixel() {
	return bytesPerPixel;
}

unsigned char* TextureData::GetPixelData() {
	return pixelData;
}

// ****************************************************************************
// Class TextureBuffer implementations

TextureBuffer::TextureBuffer() {
	tboLoaded = false;
}

TextureBuffer::TextureBuffer(TextureData *texture) {
	Load(texture);
}

TextureBuffer::~TextureBuffer() {
	Unload();
}

void TextureBuffer::Load(TextureData *texture) {
	if(tboLoaded)
		Unload();
	glGenTextures(1, &tbo);
	glBindTexture(GL_TEXTURE_2D, tbo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->GetWidth(), texture->GetHeight(),
		0, GL_RGB, GL_UNSIGNED_BYTE, texture->GetPixelData());
	tboLoaded = true;
}

void TextureBuffer::Unload() {
	if(tboLoaded)
		glDeleteTextures(1, &tbo);
	tboLoaded = false;
}

void TextureBuffer::Bind(int slot) {
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, tbo);
}
void TextureBuffer::Unbind() {
	glBindTexture(GL_TEXTURE_2D, NULL);
}

bool TextureBuffer::IsLoaded() {
	return tboLoaded;
}

// ****************************************************************************
// Class Mesh implementations

int Mesh::GetNumVerticies() {
	return verticies.size();
}

glm::vec3* Mesh::GetVerticies() {
	return verticies.data();
}

void Mesh::Load() {
	vbo.Load(GetNumVerticies(), GetVerticies());
}

void Mesh::Unload() {
	vbo.Unload();
}

void Mesh::Bind() {
	vbo.Bind();
}

void Mesh::Unbind() {
	vbo.Unbind();
}

// ****************************************************************************
// Class Material implementations

int Material::GetNumVerticies() {
	return verticies.size();
}

glm::vec4* Material::GetVerticies() {
	return verticies.data();
}

void Material::Load() {
	vbo.Load(GetNumVerticies(), GetVerticies());
}

void Material::Unload() {
	vbo.Unload();
}

void Material::Bind() {
	vbo.Bind();
}

void Material::Unbind() {
	vbo.Unbind();
}

// ****************************************************************************
// Class Texture implementations

int Texture::GetNumCooridinates() {
	return cooridinates.size();
}

glm::vec2* Texture::GetCooridinates() {
	return cooridinates.data();
}

void Texture::Load() {
	tbo.Load(data);
	vbo.Load(GetNumCooridinates(), GetCooridinates());
}

void Texture::Unload() {
	tbo.Unload();
}

void Texture::Bind(int slot) {
	tbo.Bind(slot);
	vbo.Bind();
}

void Texture::Unbind() {
	tbo.Unbind();
}

// ****************************************************************************
// Class Object implementations

void Object::LoadVbo() {
	mesh->Load();
	material->Load();
}

Mesh* const Object::GetMesh() {
	return mesh;
}

Material* const Object::GetMaterial() {
	return material;
}

Texture* const Object::GetTexture() {
	return texture;
}

GLuint Object::GetPrimitiveType() {
	return primitiveType;
}

glm::mat4 Object::GetModelMatrix() {
	return modelMatrix;
}