// Periodic Table Visualizer
// renderer.h
//

#ifndef RENDERER_H
#define RENDERER_H

#include <Windows.h>

// OpenGL and GLEW include files
#include <gl/glew.h>
#include <gl/wglew.h>
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

// GLM include files
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "context.h"
#include "shader.h"
#include "scene.h"

class OpenGLRenderer {
private:
	int windowWidth;
	int windowHeight;

	glm::mat4 projectionMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 modelMatrix;

	int attribCoord3d;
	int attribVColor;
	int attribTexCoord;

	bool initContext;
	OpenGLContext *context;
	OpenGLShader *shader;

public:
	OpenGLRenderer(void);
	OpenGLRenderer(HWND hWnd);
	~OpenGLRenderer(void);

	bool CreateContext(HWND hWnd);
	void DeleteContext(void);

	void ClearScreen();
	void SwapBuffer();
	void SetupScene(void);
	void ReshapeWindow(int w, int h);
	void DrawScene(Scene *scene);
	void DrawObject(Object *object);

	int GetWindowWidth(void);
	int GetWindowHeight(void);
};

#endif //RENDERER_H