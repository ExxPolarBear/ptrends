// Periodic Table Visualizer
// renderer.cpp
//

#include <iostream>

#include "renderer.h"

double rotAngle = 0.0f;

using namespace std;

OpenGLRenderer::OpenGLRenderer(void) {
	initContext = false;
}

OpenGLRenderer::OpenGLRenderer(HWND hWnd) {
	CreateContext(hWnd);
}

OpenGLRenderer::~OpenGLRenderer(void) {
	shader->unbind();
	delete shader;
	DeleteContext();
}

bool OpenGLRenderer::CreateContext(HWND hWnd) {
	context = new OpenGLContext(hWnd);
	if(context->IsInit() == false)
		return false;
	return true;
}

void OpenGLRenderer::DeleteContext(void) {
	delete context;
}

void OpenGLRenderer::ReshapeWindow(int w, int h) {
	windowWidth = w;
	windowHeight = h;

//	projectionMatrix = glm::mat4(1.0f);
	projectionMatrix = glm::perspective(60.0f, (float)windowWidth / (float)windowHeight, 0.1f, 10.f);
}

void OpenGLRenderer::SetupScene(void) {
	glClearColor(0.4f, 0.6f, 0.9f, 1.0f);
//	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	shader = new OpenGLShader("shader.vert", "shader.frag");

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRange(0.0f, 1.0f);

	shader->bind( );
	
	attribCoord3d = glGetAttribLocation(shader->id(), "in_Position");
	attribVColor = glGetAttribLocation(shader->id(), "in_Color");
	attribTexCoord = glGetAttribLocation(shader->id(), "in_TexCoord");
}

void OpenGLRenderer::ClearScreen() {
	glViewport(0, 0, windowWidth, windowHeight);

	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OpenGLRenderer::SwapBuffer() {
	SwapBuffers(context->GetDeviceContext());
}

void OpenGLRenderer::DrawScene(Scene *scene) {
		for(unsigned int index = 0; index < scene->GetSize(); index++) {
			DrawObject(scene->GetObject(index));
		}
}

void OpenGLRenderer::DrawObject(Object *object) {
	viewMatrix = glm::lookAt(glm::vec3(5.0f * sin(rotAngle), 2.0f * sin(rotAngle), 
		5.0f * cos(rotAngle)), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	
	int projMatrixLoc = glGetUniformLocation(shader->id(), "projectionMatrix");
	glUniformMatrix4fv(projMatrixLoc, 1, GL_FALSE, &projectionMatrix[0][0]);

	int viewMatrixLoc = glGetUniformLocation(shader->id(), "viewMatrix");
	glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, &viewMatrix[0][0]);

	int modelMatrixLoc = glGetUniformLocation(shader->id(), "modelMatrix");
	glUniformMatrix4fv(modelMatrixLoc, 1, GL_FALSE, &object->GetModelMatrix()[0][0]);
	
	glEnableVertexAttribArray(attribCoord3d);
	object->GetMesh()->Bind();
	glVertexAttribPointer(attribCoord3d, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(attribVColor);
	object->GetMaterial()->Bind();
	glVertexAttribPointer(attribVColor, 4, GL_FLOAT, GL_FALSE, 0, 0);
	
	int texEnableLoc = glGetUniformLocation(shader->id(), "enable_texture");

	if(object->GetTexture() != NULL) {
		glUniform1i(texEnableLoc, 1);

		glEnableVertexAttribArray(attribTexCoord);
		object->GetTexture()->Bind(0);
		glVertexAttribPointer(attribTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0);
		
		int texUniformLoc = glGetUniformLocation(shader->id(), "color_Texture");
		glUniform1i(texUniformLoc, 0);
	}
	else
		glUniform1i(texEnableLoc, 0);
	
	glDrawArrays(object->GetPrimitiveType(), 0, object->GetMesh()->GetNumVerticies());

	glDisableVertexAttribArray(attribCoord3d);
	glDisableVertexAttribArray(attribVColor);
	glDisableVertexAttribArray(attribTexCoord);

	if(object->GetTexture() != NULL) {
		object->GetTexture()->Unbind();
	}

	if(rotAngle > 360.0f)
		rotAngle = 0.0f;
	rotAngle = rotAngle + 0.0002f;
}

int OpenGLRenderer::GetWindowWidth(void) {
	return windowWidth;
}

int OpenGLRenderer::GetWindowHeight(void) {
	return windowHeight;
}