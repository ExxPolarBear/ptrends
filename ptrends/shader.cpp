// Periodic Table Visualizer
// shader.cpp
//

#include <gl/glew.h>
#include <gl/wglew.h>
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

#include <iostream>
#include <fstream>

#include "shader.h"

using namespace std;

char *ReadTextFile(const char *fileName) {
	char *text;
	ifstream file(fileName, ifstream::binary);

	if(file.is_open() == false) {
		return NULL;
	}

	file.seekg(0, ios::end);
	long numChars = (long) file.tellg();
	file.seekg(0, ios::beg);
	
	text = new char[numChars+1];
	if(text == NULL) {
		file.close();
		return NULL;
	}

	file.read(text, numChars);
	text[numChars] = NULL;

	file.close();

	return text;
}

static void ValidateShader(GLuint shader, const char *file = 0) {
	const unsigned int BUFFER_SIZE = 512;
	char buffer[BUFFER_SIZE];
	memset(buffer, 0, BUFFER_SIZE);
	GLsizei length = 0;

	glGetShaderInfoLog(shader, BUFFER_SIZE, &length, buffer);
	if(length > 0)
		cout << "Shader " << shader << " (" << (file?file:"") 
		<< "):" << endl << buffer;
}

static void ValidateProgram(GLuint program) {
	const unsigned int BUFFER_SIZE = 512;
	char buffer[BUFFER_SIZE];
	memset(buffer, 0, BUFFER_SIZE);
	GLsizei length = 0;

	glGetProgramInfoLog(program, BUFFER_SIZE, &length, buffer);
	if(length > 0)
		cout << "Program " << program << " Linker error: " 
		<< endl << buffer;

	glValidateProgram(program);
	GLint status;
	glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
	if(status == GL_FALSE)
		cout << "Error validating shader " << program << endl;
}

OpenGLShader::OpenGLShader() {
	isInit = false;
}

OpenGLShader::OpenGLShader(const char *vsFile, const char *fsFile) {
	isInit = false;

	Init(vsFile, fsFile);
}

void OpenGLShader::Init(const char *vsFile, const char *fsFile) {
	if(isInit)
		return;

	cout << "Using GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

	shader_vp = glCreateShader(GL_VERTEX_SHADER);
	shader_fp = glCreateShader(GL_FRAGMENT_SHADER);

	const char *vsText = ReadTextFile(vsFile);
	const char *fsText = ReadTextFile(fsFile);

	if(vsText == NULL || fsText == NULL) {
		cout << "Either vertex shader or fragment shader file not found." << endl;
		return;
	}

	glShaderSource(shader_vp, 1, &vsText, 0);
	glCompileShader(shader_vp);
	ValidateShader(shader_vp, vsFile);

	glShaderSource(shader_fp, 1, &fsText, 0);
	glCompileShader(shader_fp);
	ValidateShader(shader_fp, fsFile);

	shader_id = glCreateProgram();
	glAttachShader(shader_id, shader_vp);
	glAttachShader(shader_id, shader_fp);

	glBindAttribLocation(shader_id, 0, "in_Position");
	glBindAttribLocation(shader_id, 1, "in_Color");

	glLinkProgram(shader_id);
	ValidateProgram(shader_id);

	delete vsText;
	delete fsText;

	isInit = true;
}

OpenGLShader::~OpenGLShader() {
	unbind();

	glDetachShader(shader_id, shader_fp);
	glDetachShader(shader_id, shader_vp);

	glDeleteShader(shader_fp);
	glDeleteShader(shader_vp);
	glDeleteProgram(shader_id);
}

unsigned int OpenGLShader::id() {
	return shader_id;
}

void OpenGLShader::bind() {
	glUseProgram(shader_id);
}

void OpenGLShader::unbind() {
	glUseProgram(0);
}