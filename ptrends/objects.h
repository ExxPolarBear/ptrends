// Periodic Table Visualizer
// objects.h
//

#ifndef OBJECTS_H
#define OBJECTS_H

#include <ft2build.h>
#include FT_FREETYPE_H

#include "base.h"
#include "text.h"

#pragma comment(lib, "freetype2411.lib")

// ============================================================================
// MATERIALS
// ****************************************************************************
// SolidColor : Solid color material with variable number of verticies

class SolidColor : public Material {
public:
	SolidColor();
	SolidColor(glm::vec4 color, int nVerticies);
	~SolidColor();
};

// ****************************************************************************
// TestBoxColors : Test pattern for a box

class TestBoxColors : public Material {
public:
	TestBoxColors();
	~TestBoxColors();

	glm::vec4 GetVertex(int index);
};

// ============================================================================
// TEXTURES
// ****************************************************************************
// LoadTexture : Load a texture

class DiscreteTexture : public Texture {
public:
	DiscreteTexture();
	DiscreteTexture(TextureData* texture);
	~DiscreteTexture();

	glm::vec2 GetVertex(int index);
};

// TextTexture : Load a font

class TextTexture : public Texture {
public:
	TextTexture();
//	TextTexture(/*font*/);
	~TextTexture();
};

// ============================================================================
// MESHES
// ****************************************************************************
// BoxMesh : Its a box

class BoxMesh : public Mesh {
public:
	BoxMesh( );
	BoxMesh(glm::vec3 scale);
	~BoxMesh( );

	glm::vec3 GetVertex(int index, glm::vec3 scale);
};

// ****************************************************************************
// RectMesh : A rectangle

class RectMesh : public Mesh {
public:
	RectMesh();
	RectMesh(glm::vec2 scale);
	~RectMesh();

	glm::vec3 GetVertex(int index, glm::vec2 scale);
};

// ============================================================================
// RENDERABLE OBJECTS
// ****************************************************************************
// ColoredBox : Box object with a solid fluorescent pastel color

class ColoredBox : public Object {
public:
	ColoredBox( );
	ColoredBox(glm::vec3 scale, glm::vec3 translation, glm::vec4 color);
	~ColoredBox( );
};

// ****************************************************************************
// ColoredRect : Rectangular object with solid color

class ColoredRect : public Object {
public:
	ColoredRect();
	ColoredRect(glm::vec2 scale, glm::vec3 translation, glm::vec4 color);
	~ColoredRect();
};

// ****************************************************************************
// TestBox : Box object with test pattern (each side different color)

class TestBox : public Object {
public:
	TestBox( );
	TestBox(glm::vec3 scale, glm::vec3 translation);
	~TestBox( );
};

// ****************************************************************************
// TexturedRect : Box object with test pattern (each side different color)

class TexturedRect : public Object {
public:
	TexturedRect();
	TexturedRect(glm::vec2 scale, glm::vec3 translation,
		TextureData* tex);
	~TexturedRect();
};

// ****************************************************************************
// Text : Text object

class TextObject : public Object {
public:
	TextObject();
	~TextObject();
};

#endif //OBJECTS_H