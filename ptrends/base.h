// Periodic Table Visualizer
// Basic classes and objects for rendering
// base.h
//

#ifndef BASE_H
#define BASE_H

#include <vector>

// OpenGL and GLEW include files
#include <gl/glew.h>
#include <gl/wglew.h>
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

// GLM include files
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "buffers.h"

using namespace std;

// ============================================================================
// CLASSES
// ****************************************************************************
// TextureData : Struct containing a single texture data

class TextureData {
private:
	unsigned int width;
	unsigned int height;
	unsigned int bytesPerPixel;
	unsigned char *pixelData;

	void SetWidth(unsigned int w);
	void SetHeight(unsigned int h);
	void SetBytesPerPixel(unsigned int bpp);
	void SetPixelData(unsigned char *data);

public:
	TextureData();
	TextureData(TextureData &texture);
	TextureData(unsigned int w, unsigned int h, unsigned int bpp,
		unsigned char *data);
	~TextureData();
	
	void Set(TextureData* const texture);
	
	unsigned int GetWidth();
	unsigned int GetHeight();
	unsigned int GetBytesPerPixel();
	unsigned char* GetPixelData();
};

// ****************************************************************************
// TextureBuffer : Handle a texture buffer object

class TextureBuffer {
private:
	GLuint tbo;
	bool tboLoaded;
public:
	TextureBuffer();
	TextureBuffer(TextureData *data);
	~TextureBuffer();

	void Load(TextureData *data);
	void Unload();

	void Bind(int slot);
	void Unbind();

	bool IsLoaded();
};

// ****************************************************************************
// Mesh : Base class for all meshes

class Mesh {
protected:
	int nVerticies;
	vector<glm::vec3> verticies;
	VertexBuffer<glm::vec3> vbo;
public:
	virtual ~Mesh() {};

	int GetNumVerticies();
	glm::vec3* GetVerticies();

	void Load();
	void Unload();
	void Bind();
	void Unbind();
};

// ****************************************************************************
// Material : Base class for all materials

class Material {
protected:
	int nVerticies;
	vector<glm::vec4> verticies;
	VertexBuffer<glm::vec4> vbo;
public:
	virtual ~Material() {};

	int GetNumVerticies();
	glm::vec4* GetVerticies();

	void Load();
	void Unload();
	void Bind();
	void Unbind();
};

// ****************************************************************************
// Texture : Base class for all textures

class Texture {
protected:
	int nCooridinates;
	vector<glm::vec2> cooridinates;
	VertexBuffer<glm::vec2> vbo;

	TextureData *data;
	TextureBuffer tbo;
public:
	virtual ~Texture() {};

	int GetNumCooridinates();
	glm::vec2* GetCooridinates();

	void Load();
	void Unload();
	void Bind(int slot);
	void Unbind();
};

// ****************************************************************************
// Object : Base class for all renderable objects

class Object {
protected:
	Mesh *mesh;
	Material *material;
	Texture *texture;
	
	GLuint primitiveType;
	glm::mat4 modelMatrix;
public:
	virtual ~Object() {};

	void LoadVbo();

	Mesh* const GetMesh();
	Material* const GetMaterial();
	Texture* const GetTexture();

	GLuint GetPrimitiveType();
	glm::mat4 GetModelMatrix();
};

#endif //BASE_H