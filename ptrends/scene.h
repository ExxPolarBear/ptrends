// Periodic Table Visualizer
// scene.h
//

#ifndef SCENE_H
#define SCENE_H

#include <vector>

#include "objects.h"
#include "base.h"

using namespace std;

// ****************************************************************************
// Scene : Currently its a list of objects

class Scene {
private:
	vector<Object *> objects;
public:
	Scene();
	~Scene();

	unsigned int GetSize();
	Object *GetObject(unsigned int index);
	void AddObject(Object *newObject);
	void Clear();
};

#endif //SCENE_H