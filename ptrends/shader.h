// Periodic Table Visualizer
// shader.h
//

#ifndef SHADER_H
#define SHADER_H

class OpenGLShader {
private:
	unsigned int shader_id;
	unsigned int shader_vp;
	unsigned int shader_fp;

	bool isInit;
public:
	OpenGLShader();
	OpenGLShader(const char *vsFile, const char *fsFile);
	~OpenGLShader();

	void Init(const char *vsFile, const char *fsFile);

	void bind();
	void unbind();

	unsigned int id();
};

#endif //SHADER_H