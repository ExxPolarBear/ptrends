// Periodic Table Visualizer
// Basic classes and objects for rendering
// base.h
//

#ifndef BUFFERS_H
#define BUFFERS_H

// OpenGL and GLEW include files
#include <gl/glew.h>
#pragma comment(lib, "opengl32.lib")

// ****************************************************************************
// VertexBuffer : Handles a vertex buffer object

template <class T>
class VertexBuffer {
private:
	GLuint vbo;
	bool vboLoaded;

public:
	VertexBuffer();
	VertexBuffer(unsigned int size, T* data);
	~VertexBuffer();
	
	void Load(unsigned int size, T* data);
	void Unload();

	void Bind();
	void Unbind();

	bool IsLoaded();
};

// ****************************************************************************
// Class VertexBuffer implementations

template <class T>
VertexBuffer<T>::VertexBuffer() {
	vboLoaded = false;
}

template <class T>
VertexBuffer<T>::VertexBuffer(unsigned int size, T* data) {
	Load(size, data);
}

template <class T>
VertexBuffer<T>::~VertexBuffer() {
	Unload();
}

template <class T>
void VertexBuffer<T>::Load(unsigned int size, T* data) {
	if(vboLoaded)
		Unload();
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, size * sizeof(T), data, GL_STATIC_DRAW);
	vboLoaded = true;
}

template <class T>
void VertexBuffer<T>::Unload() {
	if(vboLoaded)
		glDeleteBuffers(1, &vbo);
	vboLoaded = false;
}

template <class T>
void VertexBuffer<T>::Bind() {
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

template <class T>
void VertexBuffer<T>::Unbind() {
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

template <class T>
bool VertexBuffer<T>::IsLoaded() {
	return vboLoaded;
}

#endif BUFFER_H