// Periodic Table Visualizer
// renderer.h
//

#ifndef CONTEXT_H
#define CONTEXT_H

#include <Windows.h>

// OpenGL and GLEW include files
#include <gl/glew.h>
#include <gl/wglew.h>
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

class OpenGLContext {
protected:
	HGLRC hRC;
	HDC hDC;
	HWND hWnd;

	bool isInit;

public:
	OpenGLContext(void);
	OpenGLContext(HWND hWnd);
	~OpenGLContext(void);

	bool CreateContext(HWND hWnd);
	void ReleaseContext(void);
	
	bool IsInit();

	HDC GetDeviceContext(void);
};

#endif //CONTEXT_H