// Periodic Table Visualizer
// scene.cpp
//

#include "scene.h"

// ****************************************************************************
// Class Scene implementations

Scene::Scene() {
	objects.clear();
}

Scene::~Scene() {
	Clear();
}

unsigned int Scene::GetSize() {
	return objects.size();
}

Object *Scene::GetObject(unsigned int index) {
//	if(index < objects.size())
		return objects[index];
// define null object
//	else 
	//	return new NullObject();
}

void Scene::AddObject(Object *newObject) {
	objects.push_back(newObject);
	objects[objects.size()-1]->LoadVbo();
}

void Scene::Clear() {
	for(unsigned int index = 0; index < objects.size(); index++)
		delete objects[index];
	objects.clear();
}