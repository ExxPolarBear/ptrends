#version 140

in vec3 pass_Color;
in vec2 pass_TexCoord;

uniform int enable_texture;
uniform sampler2D color_Texture;

out vec4 out_Color;

void main(void) {
	if(enable_texture == 1) {
		out_Color = vec4(pass_Color, 1.0)
			* texture(color_Texture, vec2(pass_TexCoord.x, 1-pass_TexCoord.y));
	}
	else {
		out_Color = vec4(pass_Color, 1.0);
	}
}
